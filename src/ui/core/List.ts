import IVsshElement from "../IVsshElement";
import BaseElement from "./BaseElement";

/**
 * 格子状列表组件v2.0
 */
export default class List extends BaseElement{
    private list:Map<number,IVsshElement|string>
    private autoIncrement=0;
    private type:ListType
    constructor(props:ListConfig){
        super()
        this.list=new Map<number,IVsshElement|string>();
        if(props.data){
            props.data.forEach(e=>{
                this.addItem(e);
            })
        }
        this.type=props.type || ListType.Grid
    }
    /**
     * 添加一个元素
     * @param item 添加的元素
     * @returns 元素索引
     */
    addItem(item:IVsshElement|string){
        let id=this.autoIncrement;
        this.list.set(id,item);
        this.autoIncrement++;
        return id;
    }

    /**
     * 添加一个元素并重新渲染
     * @param item 
     * @returns 
     */
    addItemAndReRander(item:IVsshElement|string){
        let index=this.addItem(item);
        this.render();
        return index;
    }

    /**
     * 移除一个元素
     * @param index 
     * @returns 
     */
    removeItem(index:number){
        return this.list.delete(index);
    }

    removeItemAndRerender(index:number){
        let rel=this.removeItem(index);
        this.render();
        return rel;
    }

    /**
     * 获取列表长度
     * @returns 
     */
    getLength(){
        return this.list.size;
    }
    
    beforeRender() {
        this.setInnerElement("");
        if(this.type==ListType.Grid){
            this.style.display="grid";
            this.style.gridTemplateColumns="repeat(auto-fill, 100px)";
            this.style.gridTemplateRows="repeat(auto-fill, 100px)";
            this.style.gridRowGap="5px";
            this.style.gridColumnGap="5px";
            this.style.justifyContent="center";
        }
        if(this.type==ListType.List){
            // this.style.backgroundColor="#EEE"
        }
        this.list.forEach(element => {
            let listItem =new BaseElement();
            if(this.type==ListType.List){
                // listItem.style.borderBottom="1px solid #BBB"
                listItem.style.padding="8px"
            }
            listItem.setInnerElement(element)
            this.appendChild(listItem);
        });
    }
}

export interface ListConfig{
    data?:IVsshElement[]|string[]
    type?:ListType
}

export enum ListType{
    /**
     * 格子状列表
     */
     Grid,
    /**
     * 普通列表
     */
    List,
    
}