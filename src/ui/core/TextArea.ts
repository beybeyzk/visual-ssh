import SpecialElement from "./SpecialElement";

export default class TextArea extends SpecialElement{
    textArea: HTMLTextAreaElement
    constructor(){
        super();
        this.textArea=document.createElement("textarea");
        this.textArea.style.width="100%";
        this.textArea.style.height="100%";
        this.textArea.style.resize="none";
        this.textArea.style.border="none";
        this.textArea.style.outline="none";
        this.style.padding="5px";
        this.style.backgroundColor="#FFF";
    }
    getText(){
        return this.textArea.value;
    }
    setText(v:string){
        this.textArea.value=v;
    }
    render(): HTMLElement {
        this.setHTMLElement(this.textArea)
        return super.render()
    }
}