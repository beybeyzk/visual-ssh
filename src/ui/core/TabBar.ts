import IVsshElement from "../IVsshElement";
import BaseElement from "./BaseElement";
import Icon, { IconList } from "./Icon";
import IPageBox from "./IPageBox";
import Label from "./Label";

/**
 * Tab Bar v2.5
 * 接下来总标签页不应该直接实例化这个类，而应该再写一个类，继承自这个类。这个类单纯做为通用组件
 */
export default class TabBar extends BaseElement{
    bar:BaseElement;
    tabs:Map<number,Tab>=new Map<number,Tab>()
    private autoIncrement=0;
    pageBox:IPageBox;
    // tabPage:Container
    hv:Hv=Hv.horizontal
    constructor(props:TabBarConfig){
        super();
        this.bar=new BaseElement();
        this.bar.style.display="flex";
        this.bar.style.backgroundColor="#444"
        this.bar.style.color="#EEE"
        this.pageBox=props.pageBox
        if(props.hv){
            this.hv=props.hv
        }
        // this.tabPage=props.tabPage;
    }
    /**
     * 添加 一个标签页
     * @param tab 
     * @returns 标签页的索引
     */
    addTab(tab:Tab){
        let id=this.autoIncrement;
        tab.joinBar(this);
        this.tabs.set(id,tab);
        this.autoIncrement++;
        this.render();
        return id;
    }

    /**
     * 根据索引激活标签
     * @param index 标签的索引
     */
    activated(index:number){
        this.freezeAll();
        let tab=this.tabs.get(index);
        if(tab){
            tab.activated();
        }
    }

    /**
     * 移除标签
     * @param tab 
     */
    removeTab(tab:Tab){
        let arr=Array.from(this.tabs.entries());
        let rel=arr.find(e=>{
            console.error(e[0]);
            return e.indexOf(tab)==1;
        })
        if(!rel){
            throw new Error("没有这个标签的档案")
        }
        let index=rel[0];
        if(tab.active){
            this.activated(index?(index-1):1);
        }
        // arr=arr.filter((e,i)=>i!=index);
        this.tabs.delete(index);
        this.render();
    }

    /**
     * 根据索引获取标签
     * @param index 标签的索引
     * @returns 
     */
    getTab(index:number){
        return this.tabs.get(index);
    }

    /**
     * 设置页面内容，切换标签时使用
     * @param page 页面内容
     */
    setPage(page:BaseElement){
        this.pageBox.setPage(page);
        // this.forceRerender();
        // this.tabPage.setPage(page);
    }
    /**
     * 冻结全部标签，以便切换标签
     */
    freezeAll(){
        this.tabs.forEach(e=>{
            e.freeze();
        })
    }
    beforeRender(): void {
        this.style.display="flex"
        if(this.hv==Hv.vertical){
            this.style.flexDirection="row"
            this.bar.style.flexDirection="column"
        }else{
            this.style.flexDirection="column"
            this.bar.style.flexDirection="row"
        }
        
        this.style.height="100%";
        this.bar.setInnerElement("");
        this.tabs.forEach(e=>{
            e.addEventListener("mousedown",ev=>{
                e.style.transition="transform 0s"
                e.isDroping=true;
                e.dropStartMouseX=(<MouseEvent>ev).clientX
            })
            e.addEventListener("mousemove",ev=>{
                if(e.isDroping){
                    e.style.transform=`translateX(${(<MouseEvent>ev).clientX-e.dropStartMouseX}px)`
                }
            })
            e.addEventListener("mouseout",e.moveCancel);
            e.addEventListener("mouseup",e.moveCancel);
            this.bar.appendChild(e);
        })
        this.setInnerElement(this.bar);
        // this.appendChild(new BottomLine());
        this.appendChild(this.pageBox);
    }
}

export interface TabBarConfig{
    pageBox:IPageBox
    hv?:Hv
}

export class Tab extends BaseElement{
    active=false;
    bar?:TabBar
    /**
     * 标签对应的页面内容
     */
    page:BaseElement
    icon?:Icon
    label:Label
    closeBtn:Icon
    isDroping=false
    dropStartMouseX=0
    onlyIcon:boolean
    activeBgColor="#ccc"
    canBeUserClose=true
    constructor(props:TabConfig){
        super();
        this.onlyIcon=props.onlyIcon||false;
        if(props.canBeUserClose!=undefined){
            this.canBeUserClose=props.canBeUserClose
        }
        this.label=new Label({
            text:props.name
        })
        if(props.activeBgColor){
            this.activeBgColor=props.activeBgColor
        }
        if(props.icon){
            this.icon=new Icon({
                name:props.icon
            })
        }
        this.closeBtn=new Icon({
            name:"close"
        })
        this.style.width="100px";
        this.style.textAlign="center";
        this.style.cursor="pointer";
        this.style.display="flex";
        this.style.justifyContent="space-around"
        this.style.userSelect="none"
        this.setInnerElement(this.label);
        this.active=!!props.active;
        this.bar=props.bar;
        this.addEventListener("click",this.onclick)
        this.page=props.page;
    }

    close=(e:Event | MouseEvent)=>{
        e.stopPropagation();
        this.bar?.removeTab(this);
    }
    addEventListener(type: keyof TabEventMap, listener: (e: Event|MouseEvent) => void): void {
        if(type=="dragover" || type== "drop"){
            this.page.addEventListener(type,listener);
        }else if(type=="close"){
            this.closeBtn.addEventListener("click",listener);
        } else{
            super.addEventListener(type,listener)
        }
    }

    /**
     * 将标签加进标签组
     * @param party 标签组
     */
    joinBar(party:TabBar){
        this.bar=party
    }

    onclick=()=>{
        if(this.bar){
            this.bar.freezeAll();
            this.activated();
        }else{
            throw new ReferenceError("标签脱离了组织！")
        }
    }

    /**
     * 设置标签名
     * @param name 
     */
    setName(name:string){
        this.setInnerElement(name);
    }
    /**
     * 
     * @param pageElement 
     */
    setPage(pageElement:BaseElement){
        this.page=pageElement;
        this.page.forceRerender()
    }
    /**
     * 激活本标签
     */
    activated(){
        // alert("激活")
        this.active=true;
        this.bar?.setPage(this.page);
        this.render();
        this.closeBtn.style.display="flex"
    }
    /**
     * 获取标签文本
     * @returns 标签的文字
     */
    getText(){
        return super.getText()
    }

    /**
     * 移动取消
     * @param e 
     */
    moveCancel=(e:Event|MouseEvent)=>{
        this.style.transition="transform 0.5s"
        this.isDroping=false;
        this.style.transform=`translateX(0px)`
    }
    /**
     * 设置标签文本
     * @param str 文本
     */
    setText(str:string){
        this.label.setText(str);
        this.render();
    }
    /**
     * 冻结本标签
     */
    freeze(){
        this.active=false;
        this.closeBtn.style.display="none";
        this.render();
    }
    beforeRender(): void {
        // this.notice.appendChild("");
        this.closeBtn.addEventListener("click",this.close);
        this.style.backgroundColor=this.active?this.activeBgColor:"inherit";
        if(this.icon){
            this.setInnerElement(this.icon);
            if(!this.onlyIcon){
                this.appendChild(this.label);
            }
        }else{
            this.setInnerElement(this.label);
        }
        if(this.canBeUserClose){
            this.appendChild(this.closeBtn);
        }
        
    }
}
export interface TabConfig{
    name:string
    active?:boolean
    bar?:TabBar
    page:BaseElement
    icon?:keyof IconList
    /**
     * 是否仅显示图标.仅有icon时生效.
     */
    onlyIcon?:boolean
    /**
     * 激活时背景颜色,默认#CCC
     */
    activeBgColor?:string
    /**
     * 有没有关闭按钮
     */
    canBeUserClose?:false
}

interface TabEventMap extends HTMLElementEventMap{
    "close":Event
}

/**
 * 水平垂直
 */
export enum Hv{
    horizontal,
    vertical
}

