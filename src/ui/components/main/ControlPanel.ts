import { MsgType } from "../../../IVsshStatus";
import Status from "../../../StatusManager";
import BaseElement from "../../core/BaseElement";
import IPageBox from "../../core/IPageBox";
import TabBar, { Hv, Tab, TabConfig } from "../../core/TabBar";
import MessageBar from "./MessageBar";

export default class ControlPanel extends BaseElement{
    tabbar:TabBar
    messageBar:MessageBar;
    noticeTab:PanelTab
    constructor(){
        super();
        this.messageBar=new MessageBar();
        this.tabbar=new TabBar({
            pageBox:new PanelPageBox(),
            hv:Hv.vertical
        });
        this.noticeTab=new PanelTab({
            name:"消息",
            page:this.messageBar,
            icon:"envelope",
            onlyIcon:true,
            canBeUserClose:false,
            activeBgColor:"#666"
        })
        this.tabbar.addTab(new PanelTab({
            name:"设置",
            page:new BaseElement({
                innerElement:"别急，等我把更重要的东西做完会把这个加上哒！"
            }),
            icon:"gears",
            onlyIcon:true,
            canBeUserClose:false,
            activeBgColor:"#666"
        }))
        this.tabbar.addTab(this.noticeTab)
        // this.messageBar.addMessage("我了个去")
        // this.messageBar.addMessage("我真得好烦啊啊啊啊啊")
    }
    /**
     * 推送消息
     * @param info 
     * @returns 
     */
    addMessage(title:string,info:string,type:MsgType){
        let id=this.messageBar.addMessage(title,info,type);
        this.noticeTab.addMessage();
        return id;
    }

    removeMessage(index:number){
        this.messageBar.removeMessage(index);
        this.noticeTab.removeMessage();
    }
    beforeRender(): void {
        this.setInnerElement(this.tabbar);
    }

}

class PanelTab extends Tab{
    notice=new Notice();
    constructor(props: TabConfig){
        super(props)
        this.removeEventListener("click",this.onclick);
        this.addEventListener("click",this.childOnclick);
    }
    addMessage(){
        this.notice.setCount()
    }
    beforeRender(): void {
        this.style.width="60px";
        this.style.height="60px";
        if(this.icon){
            this.icon.style.width="60px";
            this.icon.style.height="60px";
        }
        super.beforeRender();
        this.appendChild(this.notice)
    }
    childOnclick=() => {
        if(this.bar){
            if(!this.active){
                this.bar.pageBox.style.width="150px";
                this.bar.freezeAll();
                this.activated();
            }else{
                this.bar.freezeAll();
                this.bar.pageBox.style.width="0px";
            }
        }else{
            throw new ReferenceError("标签脱离了组织！")
        }
    }
    removeMessage(){
        this.notice.setCount();
    }
}

/**
 * 通知的数量 
 */
 class Notice extends BaseElement{
    setCount(){
        let count=Status.countNotice();
        this.setInnerElement(""+count);
        if(count<=0){
            this.style.display="none";
        }else{
            this.style.display="block";
        }
    }
    beforeRender(): void {
        this.style.backgroundColor="red";
        this.style.color="#FFF";
        this.style.fontSize="12px";
        this.style.height="16px";
        this.style.width="16px";
        this.style.borderRadius="8px";
        this.style.position="absolute";
        this.style.margin="10px 0 0 30px";
        this.setCount();
    }
}

class PanelPageBox extends BaseElement implements IPageBox{
    constructor(){
        super();
        this.style.backgroundColor="#252525";
        this.style.color="rgb(187,187,187)";
        this.style.width="0px";
        this.style.transition="width 0.5s"
        this.style.overflow="hidden";
    }
    setPage(page: BaseElement): void {
        this.setInnerElement(page);
    }
}