import PageBase from "../../pages/PageBase";
import TabBar from "../../core/TabBar";

/**
 * 全局标签页
 */
export default class GlobalTabBar extends TabBar{
    beforeRender(): void {
        this.style.width="100%";
        super.beforeRender();
    }
}