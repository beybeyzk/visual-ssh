import { MsgType } from "../../../IVsshStatus";
import Status from "../../../StatusManager";
import BaseElement from "../../core/BaseElement";
import Icon from "../../core/Icon";
import Label from "../../core/Label";
import List, { ListType } from "../../core/List";

/**
 * 消息栏
 */
export default class MessageBar extends BaseElement{
    msgList:List=new List({
        type:ListType.List
    });
    noTask=new NoTask()
    constructor(){
        super();
        this.style.width="150px";
        this.style.backgroundColor="#252525"
        
    }
    /**
     * 推送信息
     * @param info 
     * @returns 消息id
     */
    addMessage(title:string,info:string,type:MsgType){
        Status.addANotice();
        this.noTask.style.display="none";
        let msgBox= new MessageBox({title,info,bar:this,msgType:type})
        let id=this.msgList.addItemAndReRander(msgBox)
        msgBox.setMsgId(id);
        return id;
    }
    removeMessage(index:number){
        Status.removeANotice();
        this.msgList.removeItemAndRerender(index);
        if(this.msgList.getLength()<=0){
            this.noTask.style.display="block";
        }
    }
    beforeRender(): void {
        this.setInnerElement(this.noTask);
        this.appendChild(this.msgList);
    }
}

export class MessageBox extends BaseElement{
    infoLabel:Label
    title:BaseElement
    bar:MessageBar;
    id=-1;
    type:MsgType
    constructor(props:MessageConfig){
        super();
        this.infoLabel=new Label({
            text:props.info
        })
        this.title=new BaseElement({
            innerElement:props.title
        })
        this.infoLabel.setText(props.info);
        this.style.padding="2px";
        this.bar=props.bar;
        this.type=props.msgType
    }
    setMsgId(n:number){
        if(this.id==-1){
            this.id=n
        }
    }
    close=()=>{
        if(this.id>=0){
            this.bar.removeMessage(this.id);
        }
    }
    beforeRender(): void {
        // this.style.border="1px solid #ccc"
        // this.style.borderRadius="4px";
        this.style.backgroundColor="#38363D";
        this.style.height="45px"
        let fileName=new BaseElement({
            innerElement:this.infoLabel
        })
        if(this.type==MsgType.doing){
            fileName.appendChild(new Icon({
                name:"loading"
            }))
        }
        if(this.type==MsgType.notice){
            let close=new Icon({
                name:"close"
            })
            close.addEventListener("click",this.close)
            fileName.appendChild(close);
        }
        
        this.title.style.fontSize="12px";
        fileName.style.display="flex";
        fileName.style.lineHeight="1.6"
        this.setInnerElement(this.title)
        this.appendChild(fileName);
    }
}
interface MessageConfig{
    title:string,
    info:string,
    bar:MessageBar,
    msgType:MsgType
}

class NoTask extends BaseElement{
    beforeRender(): void {
        let bg=new NoTaskBgLine();
        let text=new NoTaskText();
        this.style.padding="5px";
        this.setInnerElement(bg);
        this.appendChild(text);
    }
}

class NoTaskBgLine extends BaseElement{
    beforeRender(): void {
        this.style.border="1px solid rgb(187,187,187)"
        this.style.transform="translateY(10px)"
    }
}
class NoTaskText extends BaseElement{
    beforeRender(): void {
        this.style.fontSize="12px";
        this.style.width="120px";
        this.style.margin="auto";
        this.style.position="relative";
        this.style.textAlign="center";
        this.style.backgroundColor="#252525"
        this.setInnerElement("当前无进行中的任务");
    }
}