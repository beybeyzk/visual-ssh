import { ClipBoardData, ClipBoardOption } from "./IVsshStatus";

/**
 * 全局状态管理v0.1
 */
export class StatusManager{
    private expect=""
    /**
     * 剪切板。现阶段只有string。
     */
    private ClipBoard=""

     /**
      * 剪切板类型
      */
    private ClipBoardOption:ClipBoardOption=ClipBoardOption.Copy

    /**
     * 当前登录的用户名
     */ 
    private username:string=""
    private password:string=""
    private host:string="127.0.0.1"
    private port:string |number=0
    private privateKey?:Buffer

    private noticeCount=0;
    constructor(){}

    /**
     * 计加一条通知
     */
    addANotice(){
        this.noticeCount++;
    }

    /**
     * 将数据以复制模式加入剪切板
     * @param data 
     */
    copy(data:string){
        this.ClipBoard=data;
        this.ClipBoardOption=ClipBoardOption.Copy
    }

    /**
     * 获取通知数
     * @returns 
     */
    countNotice(){
        return this.noticeCount;
    }

    /**
     * 将数据以剪切模式加入剪切板
     * @param data 
     */
    cut(data:string){
        this.ClipBoard=data;
        this.ClipBoardOption=ClipBoardOption.Cut;
    }

    /**
     * 可输入命令的标志物，类似于[root@VM_0_6_centos ~]# iven@iven-Lenovo-TianYi-310-15ISK:~$ 
     * @returns 
     */
    getExpect(){
        return this.expect;
    }

    getHost(){
        return this.host;
    }

    getPassword(){
        return this.password
    }

    getPort(){
        return this.port
    }

    getPrivateKey(){
        if(this.privateKey){
            return this.privateKey
        }
        throw Error("密钥呢???")
    }

    getUsername(){
        return this.username
    }

    paste():ClipBoardData{
        return {
            data:this.ClipBoard,
            option:this.ClipBoardOption
        };
    }

    /**
     * 计减一条通知
     */
    removeANotice(){
        this.noticeCount--
    }

    /**
     * 设置标识
     * @param v 
     */
    setExpect(v:string){
        this.expect=v;
    }

    setHost(addr:string){
        this.host=addr
    }

    setPassword(pass:string){
        this.password=pass
    }

    setPort(p:number){
        this.port=p;
    }

    setPrivateKey(key:Buffer){
        this.privateKey=key;
    }

    setUsername(name:string){
        this.username=name
    }
}

let Status=new StatusManager()
export default Status;