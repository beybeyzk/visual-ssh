import App from "../../App";
import Program from "../../Program";
import { Tab } from "../../ui/core/TabBar";
import AppStart from "./AppStart";
import AppStartParser from "./AppStartParser";

export default class AppManagerApp extends App{
    version: string="0.1";
    constructor(rt:Program){
        super(rt);
    }
    async onStart() {
        this.page=new AppStart({
            runtime:this.runtime,
            container:this.runtime.getContainer()
        })
        let tabId=this.runtime.addNewTab({
            name:'应用管理器',
            page:this.page
        })
        let tab=this.runtime.getTab(tabId);
        if(!tab){
            throw new Error("获取标签失败")
        }
        let connectId=await this.runtime.createConnect(tab,(conn,tab)=> new AppStartParser());
    }
}