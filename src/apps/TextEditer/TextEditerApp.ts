import App, { AppRunConfig } from "../../App";
import FileEditParser from "./FileEditParser";
import TextEditer from "./TextEditer";

export default class TextEditerApp extends App{
    version: string="0.1";
    onStart(config:AppRunConfig): void {
        this.appName="文本编辑器"
        this.page=new TextEditer({
            path:config.path
        });
        let connId=this.createConnect((conn,tab)=>{
            let parser=new FileEditParser(conn,tab,this.runtime.getContainer());
            parser.setPage(this.page);
            if(config.path){
                parser.setPath(config.path);
            }
            return parser
        });
    }
}