import BaseElement from "../../ui/core/BaseElement";
import Button from "../../ui/core/Button";
import TextArea from "../../ui/core/TextArea";

export default class TextEditer extends BaseElement{
    filePath?:string
    inputBox:TextArea
    btn:Button
    constructor(props:EditerConfig){
        super();
        this.filePath=props.path;
        this.style.display="flex";
        this.style.flexDirection="column"
        this.style.height="100%";
        this.inputBox=new TextArea();
        this.inputBox.style.width="100%";
        this.inputBox.style.flexGrow="1";
        this.btn=new Button({
            text:"保存"
        })
    }
    addEventListener(type: keyof FileEditEventMap, listener: (this: HTMLDivElement, e: Event | MouseEvent) => any): void {
        if(type=="save"){
            this.btn.addEventListener("click",listener)
        }else{
            super.addEventListener(type,listener);
        }
    }
    getText(): string {
        return this.inputBox.getText();
    }
    setText(v:string){
        this.inputBox.setText(v);
    }
    beforeRender(): void {
        this.setInnerElement(this.inputBox);
        let bottom=new BaseElement({
            innerElement:this.btn
        })
        bottom.style.flexGrow="0"
        bottom.style.flexShrink="0"
        this.appendChild(bottom);
    }
}
interface EditerConfig{
    path?:string;
}
interface FileEditEventMap extends HTMLElementEventMap{
    "save":Event
}