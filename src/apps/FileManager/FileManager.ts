import IVsshElement from "../../ui/IVsshElement";
import NavBar, { NavBarElementEventMap } from "./NavBar";
import BaseElement from "../../ui/core/BaseElement";
import { FileType, MsgType } from "../../IVsshStatus";
import FileIcon from "../../ui/components/FileIcon";
import LsParser from "../../parser/LsParser";
import { RightMenuListItem } from "../../ui/components/RightMenuListItem";
import List from "../../ui/core/List";
import { DFile } from "../../VirtualFileSystem";
import Loading from "../../ui/core/Loading";
import TextEditerApp from "../TextEditer/TextEditerApp";
import Status from "../../StatusManager";
import FileInput from "../../ui/core/FileInput";
import scp from "scp2";
import Tools from "../../Tools";

export default class FileManager extends BaseElement{
    renderArea:BaseElement
    navBar:NavBar
    paser:LsParser
    loading:Loading
    upload:FileInput
    constructor(lsPaser:LsParser){
        super();
        this.paser=lsPaser;
        this.style.display="flex";
        this.style.height="100%";
        this.style.flexDirection="column";
        this.renderArea=new BaseElement({});
        this.renderArea.style.overflowY="scroll";
        this.renderArea.style.height="100%";
        this.navBar=new NavBar({
            url:"~"
        })
        this.loading=new Loading({
            type:"LoadingTest2"
        })
        this.setPage(this.loading);
        this.upload=new FileInput({})
        this.navBar.addEventListener("upload",this.clickUpload)
        this.upload.addEventListener("selectedFile",()=>{
            let uploadId=lsPaser.addMessage("上传文件",Tools.getFileNameFormUri(this.upload.getValue()),MsgType.doing);
            scp.scp(this.upload.getValue(),{
                host: Status.getHost(),
                username: Status.getUsername(),
                password: Status.getPassword(),
                path: this.getUrl(),
                port:Status.getPort()
            },(e)=>{
                if(e){
                    alert("发生错误");
                    console.error(e)
                }else{
                    this.ls();
                    lsPaser.removeMessage(uploadId);
                    lsPaser.addMessage("上传完成",Tools.getFileNameFormUri(this.upload.getValue()),MsgType.notice);
                }
                
            })
            alert(this.upload.getValue());
        })
    }
    addEventListener(type: keyof NavBarElementEventMap, listener: (e: Event) => void): void {
        if(type=="last" || type =="back" || type == "root" || type =="home" || type == "upload"){
            this.navBar.addEventListener(type,listener);
        }else{
            super.addEventListener(type,listener);
        }
    }
    clickUpload=()=>{
        this.upload.click();
    }
    getUrl(){
        return this.navBar.getUrl();
    }
    /**
     * 显示当前目录全部文件
     */
    ls(){
        let data =this.paser.getCurrentFileList();
        this.setData(data);
    }

    /**
     * @deprecated
     * 在文本编辑器中打开。以后应该有个统一注册打开方式的地方。
     * @param path 文件完整路径
     */
    openInTextEditer=(path:string)=>{
        let app=new TextEditerApp(this.paser.getConnect().getProgram());
        app.onStart({
            path
        })
    }
    setData(data:DFile[]){
        let icons: FileIcon[]=data.map((item,i,arr)=>{
            let icon=new FileIcon({
                name:item.name,
                type:item.getType()
            })
            if(item.getType()==FileType.folder){
                icon.addEventListener("dblclick",async (e)=>{
                    await this.paser.cd(item.getFullPath());
                    this.ls();
                })
            }else if(item.getType()==FileType.text){
                icon.addEventListener("dblclick",async (e)=>{
                    await this.openInTextEditer(item.getFullPath());
                })
            }
            icon.addEventListener("mousedown",(e)=>{
                let ev=<MouseEvent>e;
                e.stopPropagation()
                if(ev.button ==2){
                    let copy=new RightMenuListItem({
                        icon:"copy",
                        title:"复制"
                    });
                    copy.addEventListener("click",(e)=>{
                        Status.copy(`${this.getUrl()}/${item.name}`)
                    })
                    let cut=new RightMenuListItem({
                        icon:"cut",
                        title:"剪切"
                    })
                    cut.addEventListener("click",e=>{
                        Status.cut(`${this.getUrl()}/${item.name}`)
                    })
                    let paste=new RightMenuListItem({
                        icon:"paste",
                        title:"粘贴"
                    })
                    paste.addEventListener("click",this.paser.onParse)

                    let edit=new RightMenuListItem({
                        icon:"edit",
                        title:"编辑"
                    })
                    edit.addEventListener("click",async (e)=>{
                        await this.openInTextEditer(item.getFullPath());
                    })
                    
                    let items:Array<RightMenuListItem>=[]
                    items.push(copy);
                    items.push(cut);

                    let clipBoardData=Status.paste()
                    // 那么，剪切过一个文件，如果粘贴再次会怎样呢？？？
                    if(clipBoardData?.data){
                        items.push(paste);
                    }
                    if(item.getType()==FileType.text){
                        items.push(edit);
                    }
                    let menu=this.paser.createRightMenu({
                        x:ev.x,
                        y:ev.y,
                        data:items,
                    })
                }
            })
            return icon
        })
        console.warn("要被渲染的",icons);
        let ele=new List({data:icons});
        this.setPage(ele);
        this.loading.hidden()
    }
    setPage(pageEle:IVsshElement){
        this.renderArea.setInnerElement(pageEle);
    }
    setUrl(url:string){
        this.navBar.setUrl(url);
    }
    beforeRender(): void {
        this.setInnerElement(this.navBar);
        this.appendChild(this.renderArea);
    }
}