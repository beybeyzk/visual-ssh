import BaseElement from "../../ui/core/BaseElement";
import Button from "../../ui/core/Button";
import FormInput from "../../ui/core/FormInput";

export default class NavBar extends BaseElement{
    url=""
    backButton:Button;
    lastButton:Button;
    homeButton:Button;
    rootButton:Button;
    upload:Button;
    urlBox:FormInput;
    constructor(props:NavBarConfig){
        super();
        this.backButton=new Button({
            icon:"arrowLeft",
            title:"cd -"
        })
        this.lastButton=new Button({
            icon:"arrowUp",
            title:"cd .."
        })
        this.homeButton=new Button({
            icon:"home",
            title:"cd ~"
        })
        this.rootButton=new Button({
            icon:"database",
            title:"cd /"
        })
        this.upload=new Button({
            icon:"upload",
            title:"上传文件"
        })
        this.urlBox=new FormInput({
            value:props.url
        })
    }
    setUrl(url:string){
        this.urlBox.setValue(url);
    }
    getUrl():string{
        return this.urlBox.getValue();
    }
    addEventListener(type: keyof NavBarElementEventMap, listener: (e: Event) => void): void {
        if(type=="back"){
            this.backButton.addEventListener("click",listener);
        }
        if(type=="last"){
            this.lastButton.addEventListener("click",listener);
        }
        if(type=="root"){
            this.rootButton.addEventListener("click",listener);
        }
        if(type=="home"){
            this.homeButton.addEventListener("click",listener);
        }
        if(type == "upload"){
            this.upload.addEventListener("click",listener);
        }
    }
    beforeRender(): void {
        this.style.borderBottom="solid 1px #999999"
        this.style.display="flex"
        this.setInnerElement(new BtnBox(this.backButton))
        this.appendChild(new BtnBox(this.lastButton));
        this.appendChild(this.urlBox);
        this.appendChild(new BtnBox(this.homeButton));
        this.appendChild(new BtnBox(this.rootButton));
        this.appendChild(new BtnBox(this.upload));
    }
}

class BtnBox extends BaseElement{
    constructor(btn:Button){
        super({innerElement:btn});
        this.style.margin="8px 0 8px 8px";
    }
}

export interface NavBarConfig{
    url:string
}

export interface NavBarElementEventMap extends HTMLElementEventMap{
    back:Event;
    last:Event;
    home:Event;
    root:Event;
    upload:Event;
}