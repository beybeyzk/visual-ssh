import App, { AppRunConfig } from "../../App";
import LsParser from "../../parser/LsParser";
import Loading from "../../ui/core/Loading";

export default class FileManagerApp extends App{
    version: string="0.1";
    async onStart(config:AppRunConfig) {
        // this.page=new FileManager()
        this.page.setInnerElement(new Loading({
            type:"LoadingTest2"
        }))
        let tab=config.tab
        if(!tab){
            let tabId=this.runtime.addNewTab({
                name:'文件管理器',
                page:this.page,
                active:false
            })
            tab=this.runtime.getTab(tabId);
        }
        if(!tab){
            throw new Error("标签未注册");
        }
        tab.setName("文件管理器")
        let connectId=await this.runtime.createConnect(tab,(conn,tab)=> new LsParser(conn,tab,this.runtime.getContainer()));
    }
}