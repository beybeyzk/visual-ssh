import { ClientChannel } from "ssh2";
import IVsshStatus, { ClipBoardData, ClipBoardOption, FileType, IDFile } from "./IVsshStatus";

import Container from "./ui/components/main/Container";
import Connect from "./Connect";
import { Tab, TabConfig } from "./ui/core/TabBar";
import IParser from "./parser/IParser";
import VirtualFileSystem from "./VirtualFileSystem";

export default class Program{
    private Status:IVsshStatus;
    privateKey:Buffer
    connectPool:Map<number,Connect>=new Map<number,Connect>();
    private autoIncrement=0;
    private fileSystemCache:VirtualFileSystem=new VirtualFileSystem();
    username:string;
    password;
    host;
    port;
    constructor(config:RuntimeConfig){
        this.username= config.username,
        this.password= config.password,
        this.host= config.host,
        this.port= config.port,
        this.Status={
            lineTitleVerified: false,
            container: config.container,
            dataLock: false,
        }
        this.privateKey=config.privateKey;
        // this.run();
    }

    /**
     * 新建标签页
     * @param props 标签页设置
     * @returns 标签索引
     */
    addNewTab(props:TabConfig){
        return this.Status.container.addNewTab(props);
    }

    /**
     * 添加一个已经实例化的标签页
     * @param tab 
     * @returns 标签页索引
     */
    addTab(tab:Tab){
        return this.Status.container.addTab(tab);
    }

    /**
     * 测试连接
     * @returns 
     */
    async connectTest(){
        return await Connect.testConnect({
            username:this.username,
            password:this.password,
            host:this.host,
            port:Number(this.port),
            privateKey:this.privateKey,
        })
    }

    async createConnect(page:Tab,parserCreator:(conn:Connect,tab:Tab)=>IParser){
        let id=this.autoIncrement;
        let l= this.connectPool.set(id,await Connect.create({
            username:this.username,
            password:this.password,
            host:this.host,
            port:Number(this.port),
            privateKey:this.privateKey,
            renderPage:page,
            mainProgram:this,
            parserCreator
        }))
        this.autoIncrement++;
        console.warn("创建了连接")
        return id
    }

    

    getContainer(){
        return this.Status.container;
    }

    /**
     * 获取标签的索引
     * @param index 标签的id
     * @returns 
     */
    getTab(index:number){
        return this.Status.container.getTab(index);
    }

    /**
     * 获取文件系统缓存
     * @returns 
     */
    getVirtualFileSystem(){
        return this.fileSystemCache;
    }
}

export interface RuntimeConfig{
    container:Container,
    username:string,
    password:string
    host:string
    port:string |number
    privateKey:Buffer,
}