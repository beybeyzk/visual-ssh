
import Container from "./ui/components/main/Container";

/**
 * @deprecated
 */
export default interface IVsshStatus{
    /**
     * 可输入命令的标志物需要系统自动判断。因为最先出现的玩意儿它不是。
     */
    lineTitleVerified:boolean
    
    /**
     * 根元素
     */
    container:Container
    /**
     * 数据锁，当数据锁上锁时等待解锁再执行操作；
     */
    dataLock:boolean
    
}

/**
 * 文件类型
 */
export enum FileType{
    folder,
    exe,
    other,
    image,
    text
}

/**
 * 剪切板原操作类型
 */
export enum ClipBoardOption{
    Copy,
    Cut
}

export interface ClipBoardData{
    data:string
    option:ClipBoardOption
}

/**
 * 文件／文件夹列表项
 */
export interface IDFile{
    parent?:IDFile
    name:string,
    type:FileType
    children?:IDFile[]
}

/**
 * 消息类型
 */
export enum MsgType{
    doing,
    notice
}
