import Connect from "./Connect";
import IParser from "./parser/IParser";
import Program from "./Program";
import BaseElement from "./ui/core/BaseElement";
import { Tab } from "./ui/core/TabBar";

/**
 * 扩展应用API v0.2
 */
export default class App{
    version = "1.0"
    /**
     * 应用名称。这个会显示在标签页上
     */
    appName="示例应用"
    /**
     * 界面
     */
    page=new BaseElement()
    runtime:Program
    constructor(rt:Program){
        this.runtime=rt
    }
    /**
     * 应用启动时会自动调用这个方法
     * @param tab 应用所占用的标签页
     */
    onStart(config:AppRunConfig){
        throw new Error("没有重写onStart方法。它会在应用启动时自动调用");
    }
    createTab(){
        return this.runtime.addNewTab({
            name:this.appName,
            page:this.page
        })
    }
    createAndGetTab(){
        let tabId=this.createTab();
        let id=this.runtime.getTab(tabId);
        if(!id){
            throw new Error("标签创建失败")
        }
        return id;
    }
    /**
     * 创建连接。
     * @param parserCreator 
     * @returns 
     */
    async createConnect(parserCreator: (conn: Connect, tab: Tab) => IParser){
        let tab=this.createAndGetTab();
        return await this.runtime.createConnect(tab,parserCreator);
    }
}
export interface AppRunConfig{
    tab?:Tab,
    path?:string;
}